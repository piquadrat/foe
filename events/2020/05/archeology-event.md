# Archeology event
## Main Questline
* Quest 1: "Spend some (12 - 45) Forge Points"
* Quest 2: "Defeat some (10 - 42) units OR Solve this simple negotiation"
* Quest 3: "In a production building, finish a 5-minute production some (4 - 24) times"Reward:  "Gather some (20 - 220) goods e.g. From goods buildings or by trading"
* Quest 4: "Build 1 production building from your age OR Build 2 production buildings from the previous age"Reward:  "Buy 5 Forge Points"
* Quest 5: "Visit 20 Friends Taverns OR Motivate or polish 50 buildings of other players"
* Quest 6: "Gain some (230 - 30,000) happiness"Reward:  "Pay some (1,400 - 1,200,000) supplies"
* Quest 7: "Win some (4 - 8) battles without losing OR Donate some (50 - 430) goods to the guild treasury"
* Quest 8: "Spend some (12 - 45) Forge Points"Reward:  "Gather some (1,500 - 1,650,000) coins"
* Quest 9: "Gather some (800 - 1,050,000) supplies"
* Quest 10: "Collect 750 Tavern Silver OR Recruit 3 units from your era"Reward:  "Gather some (20 - 220) goods e.g. From goods buildings or by trading"
* Quest 11: "Collect 3 incidents OR Exchange 2 items in the Antiques Dealer building"Reward:  "In a production building, finish a 15-minute production some (3 - 18) times"
* Quest 12: "Defeat some (10 - 45) units OR Donate some (30 - 260) goods to the guild treasury"
* Quest 13: "Build a goods building from your age OR Recruit 4 units from your era"Reward:  "Motivate or polish 20 buildings of other players"
* Quest 14: "Defeat this small army OR Solve this moderate negotiation"Reward:  "Spend some (12 - 45) Forge Points"
* Quest 15: "In a production building, finish a 5-minute production some (5 - 30) times"
* Quest 16: "Visit 20 Friends Taverns OR Defeat some (10 - 45) units"Reward:  "Gain some (230 - 30,000) happiness"
* Quest 17: "Collect 3 incidents OR Contribute some (30 - 113) Forge Points to Great Buildings"Reward:  "Pay some (2,100 - 1,800,000) supplies"
* Quest 18: "Build 2 production buildings from your age OR Build 3 production buildings from the previous age"
* Quest 19: "Activate 2 boosts in the Friends Tavern OR Solve this moderate negotiation"
* Quest 20: "In a production building, finish a 4-hour production some (4 - 24) times"Reward:  "Gather some (1,000 - 1,400,000) supplies"
  * Reward: Portrait of Conrad Portrait 367 
* Quest 21: "Gather some (24 - 264) goods e.g. From goods buildings or by trading OR Donate some (40 - 340) goods to the guild treasury"Reward:  "Gain some (230 - 30,000) happiness"
* Quest 22: "Defeat this medium-sized army OR Solve this moderate negotiation"
* Quest 23: "Gather some (800 - 1,050,000) supplies"Reward:  "Buy 5 Forge Points"
* Quest 24: "Spend some (12 - 45) Forge Points"Reward:  "Pay some (2,400 - 2,899,200) coins"
* Quest 25: "Exchange 3 items in the Antiques Dealer building OR In a production building, finish a 8-hour production some (2 - 12) times"
* Quest 26: "Build a goods building from your age OR Recruit 4 units from your era"Reward:  "Gather some (20 - 220) goods e.g. From goods buildings or by trading"
* Quest 27: "Gather some (800 - 1,050,000) supplies"Reward:  "Spend some (18 - 68) Forge Points"
* Quest 28: "Visit 20 Friends Taverns or polish 50 buildings of other players"Reward:  "In a production building, finish a 4-hour production some (2 - 12) times"
* Quest 29: "Collect 5 incidents OR Finish 10 productions in a production buildings from your age"Reward:  "Pay some (2,000 - 2,416,000) coins"
* Quest 30: "Gain control over a province OR Solve 12 encounters in the Guild Expeditions"
* Quest 31: "In a production building, finish an 8-hour production some (4 - 24) times"Reward:  "Spend some (14 - 54) Forge Points"
* Quest 32: "Build 2 residential buildings from your age OR Build 3 residential buildings from the previous age"Reward:  "Gather some (1,000 - 1,400,000) supplies"
* Quest 33: "Exchange 5 items in the Antiques Dealer building OR Gather some (50 - 550) goods e.g. From goods buildings or by trading"
* Quest 34: "Defeat this medium-sized army OR Solve this complex negotiation"
  * Reward:  a Portrait of Johanna Portrait 368 
* Quest 35: "Spend 500 Tavern Silver in the Friends Tavern OR Buy 10 Forge Points"Reward:  "Pay some (2,000 - 2,200,000) coins"
* Quest 36: "Defeat this small army OR Solve this moderate negotiation"Reward:  "Motivate or polish 10 buildings of other players"
* Quest 37: "Win some (3 - 12) battles OR Donate some (40 - 340) goods to the guild treasury"
* Quest 38: "Build 1 goods building from your age OR Gather some (50 - 550) goods e.g. From goods buildings or by trading"Reward:  "In a production building, finish a 5-minute production some (2 - 12) times"
* Quest 39: "Activate 2 boosts in the Friends Tavern OR Win some (3 - 6) battles without losing"Reward:  "Pay some (4,000 - 4,832,000) coins"

## Daily Quests

* Quest 40 - Day 1: "Gather some (900 - 1,260,000) supplies" and "Gather some (1,500 - 1,650,000) coins"
* Quest 41 - Day 2: "Win some (3 - 6) battles without losing OR Collect 1200 Tavern Silver" and "In a production building, finish a 4-hour production some (4 - 24) times"
* Quest 42 - Day 3: "Build 4 decorations from your age or 6 from the previous age" and "Motivate or polish 30 buildings of other players"
* Quest 43 - Day 4: "Solve this moderate negotiation OR Recruit 3 units from your age"
* Quest 44 - Day 5: "Finish 10 productions in a production buildings from your age OR Finish 12 productions in a production buildings from the previous age" and "Gather some (20 - 220) goods e.g. From goods buildings or by trading"
* Quest 45 - Day 6: "Collect 3 incidents OR Visit 15 Friends Taverns" and "Pay some (1,700 - 1,440,000) supplies"
* Quest 46 - Day 7: "Spend 450 Tavern Silver in the Friends Tavern OR In a production building, finish a 15-minute production some (4 - 22) times" and "Spend some (24 - 90) Forge Points"
* Quest 47 - Day 8: "Gain some (230 - 30,000) happiness" and "Gather some (1,000 - 1,100,000) coins"
* Quest 48 - Day 9: "Gather some (40 - 440) goods e.g. From goods buildings or by trading OR Donate some (50 - 430) goods to the guild treasury"
* Quest 49 - Day 10: "Have the first difficulty in the Guild Expedition solved OR Defeat this large army"
* Quest 50 - Day 11: "Activate 2 boosts in the Friends Tavern OR Win some (3 - 6) battles without losing" and "Pay some (5,000 - 6,040,000) coins"
* Quest 51 - Day 12: "Finish 15 productions in a production buildings from your age OR Finish 18 productions in a production buildings from the previous age" and "Spend some (12 - 45) Forge Points"
* Quest 52 - Day 13: "Build 1 production building from your age OR Build 2 production buildings from the previous age" and "Gather some (1,000 - 1,400,000) supplies"
* Quest 53 - Day 14: "Recruit 5 units from your age OR Gather some (42 - 462) goods e.g. From goods buildings or by trading" and "Gather some (1,500 - 1,650,000) coins"
* Quest 54 - Day 15: "Defeat this medium-sized army OR Solve this moderate negotiation"
* Quest 55 - Day 16: "Visit 12 Taverns OR Motivate or polish 50 buildings of other players" and "Gain some (45 - 22,500) total population"
* Quest 56 - Day 17: "Collect 3 incidents OR Visit 15 Friends Taverns" and "Pay some (2,800 - 2,400,000) supplies"
* Quest 57 - Day 18: "In a production building, finish a 4-hour production some (4 - 24) times OR Exchange 4 items in the Antiques Dealer building" and "Pay some (5,000 - 6,040,000) coins"
* Quest 58 - Day 19: "Build 1 culture building from your age OR Build 2 culture buildings from the previous age" and "In a production building, finish a 8-hour production some (2 - 12) times"
* Quest 59 - Day 20: "Acquire some (2 - 3) sectors OR Defeat some (20 - 90) units"
* Quest 60 - Day 21: "Activate 2 boosts in the Friends Tavern OR Win some (3 - 6) battles without losing" and "Pay some (5,000 - 6,040,000) coins"
  * Reward: Scrolls 100 Scrolls 

## Event building
The event building is a an Airship sized 7x3 filed

Maxed out, it will provide

* 9fp
* 20goods to player
* 20 goods to treasurey 

Depending on players era

* population
* happiness (more than population)
* gold
* supplies

## General recommendation,

* If you buy tools for your scrolls, always wait until you can afford the biggest package
* Focus on getting the statues, don't invest too much to dig out other artifacts

If you do that, finish all quests and are conscious about how you use the tools, I think you should easily be able to get a maxed out airship.
