# 1.9 Support Thread

## Idea
Those players with a high level Arc give back all bonus-fps they get to the player whose building they help with.
The supporting player will get some bps and some medals, but no surplus in fp.

For higher level GBs, the supported player only needs to invest a small fraction of the total cost to level. E.g. to level an Arc
from level 46 to 47 would normally cost 2,419fp. However, with a 1.9 support thread, the owner only has to pay 42fp himself.

Online tools like http://graldron.bplaced.net/ help with the calculation, so it is really easy to use.

## Example
There is a lot of calculation in this, but fear not, online tools like http://graldron.bplaced.net/ will do all those calculations
for you.

Peter has an Arc level 27 and wants to put it to an 1.9 support thread. Alice, Bob, Chris, 
Eric and Fred have all level 80 Arcs (90% reward bonus)

* To level from 27 to 28 takes 1,513 forge points
* Alice is willing to take place 1. Since he will get 385fp reward + 347fp reward bonus = 732fp, she is 
willing to pay 732 to secure 1st place
* Since 732 x 2 = 1,464 is less than the 1,513, 1st place is only secure once Peter added the difference of 49 fp himself.
* Now Alice can secure 1st place, Bob can secure 2nd, etc.

The sequence is therefore:
1. Peter pays 49fp himself
2. Alice pays 1464fp
3. Bob pays 371fp
4. Peter adds 113fp himself
5. Chris pays 124fp
6. Peter adds another 66fp himself
7. Eric pays 29 fp
8. Peter adds another 10fp himself
9. Fred pays 10fp
10. Peter can level for another 9fp

## Fair behaviour in 1.9 threads
* ***Secure the spots you are asking for before posting the building***. In the example, Peter should add 49fp before posting
his Arc and asking for 1st and 2nd to be taken, and also add the 113 before asking again for 3rd
* ***Do the math yourself***, e.g. ask for "Peter Arc p1 (732)"
* ***Copy open requests when posting your own*** e.g. if "Olaf Arc p2 (649)" is open, Peter should post "Peter Arc p1 (732), Olaf Arc p2 (649)"
* ***Add your name to the request*** This helps when requests are copied down
* ***Announce before you contribute*** to avoid two players trying to get the same place at the same moment (yes, it happens, yes, fps are lost that way)
* ***Don't snipe*** Sometimes it is possible to get 1st place for less than 1.9 support. Don't do that. This thread is meant 
to support guild mates (or friends). You can snipe in your neighbourhood all you want, those are enemies / competition.

