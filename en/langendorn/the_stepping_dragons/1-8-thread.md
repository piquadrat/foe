# 1.8 Thread
## TL/DR (Too long, don't read, a short summary)

* Owners who ask for 1.8 support must make sure the contributors get a safe place for their investment
* Owner tries to level within 3 days. Owners expecting it to take longer should state so when posting their building
* Contributors post which place they intent to take, before conrtibuting
* Contributors must contribute 1.8 x the expected reward, even if they could secure a place for less
* Contributors shoud calculate if the place they take is secure, before investing.
* Anyone noticing a posted building doesn't match the requirement should point it out
in the thread

There are tools online to do the math:

https://foe.tools/gb-investment/

## Verbose Version
### Purpose
The purpose of this thread is to help guild-mates to improve their GBs faster, while
allowing a small profit to those owning a high-level Arc. It helps to prevent 
snipers outside the guild and to make sure the owner of a GB gets what the position
is worth. The advantage:

e.g. if you have an lv20 Arc and want to bring it to lv21, it would cost you 
1273fp. With 1.8 support thread, you would get 828fp for free, reducing your costs to
only 445.

For contributors, if they invested in a high level Arc (>lv60), they can turn a small 
profit in forgepoints, but also gain blueprints and medals.

### GB Owner
The owner can post a building and ask for certain places to be taken, 
if the highest available position will be safe. E.g.:

    Arc, lv 20, 1st

#### Example for an ***unsafe*** spot:

* The owner has a lv20 Arc. Next level will cost a total of 1273fp
* Contribution reward for 1st place would be 270fp.
* The contributor is expected to invest 270 * 1.8=486 FP.

*This means, if nothing was invested before, and the contributer puts 486fp, 
a neighbour / friend / guild mate could still steal 1st place. It was not safe 
and should not have been posted.*

#### Example for a ***safe*** spot:

* The owner has a lv20 Arc. Next level will cost a total of 1273fp
* The owner adds 301fp before posting it
* The contributor invests 270 * 1.8=486 FP

*The total is now 787fp, no one can add more than 486fp anymore, 
the 1st place is safe.*

**The owner must always make sure the position asked for to be taken is secure for a 
1.8 contributor. This avoids tedious arguments in case a sniper from the 
neighbourhood steals 1st place and contributors lose their expected reward.
Owners who ask for insecure places too often might be removed from the thread to 
avoid long-term frustration and arguments**

Following link will help with the math: https://foe.tools/gb-investment/

Further, it is expected the owner will actively work on the posted building (not leaving it 
hanging for weeks), so contributors get their rewards soon enough.

### Contributor
Before contributing, the contributor should state the intention in the thread, like

    Taking 1st on jakudzax Arc

This avoids conflicts of two contributors adding fps at the same time.

A contributor should always contribute 1.8 times the expected contribution-reward.

E.g.: 
* An owner posts a lv40 Arc. Required amount to level: 2086fp
* Expected reward for 1st place: 615fp. Expected contribution: 615*1.8=1107fp
* The contributor could secure 1st place for 1043fp, but must put 1107fp instead.

*** Hint: In such cases, it is recommended to put only 1043 first, until 2nd was also secured, to 
avoid encouraging snipers. However, if you do that, please pay attention to put the
rest of your contribution timely after 2nd was secured, to avoid frustration and 
misunderstanding. ***

*A contributor should always counter-check if the position he takes is actually safe, 
before contributing. It prevents tedious arguments about claiming reimbursement 
from GB owner or similar problems.*

### 1.8 vs 1.9
This guild has one 1.8 and another 1.9 thread.

The 1.9 Thread

* Gives better/faster support
* Has restricted access (rules will be published in that thread)
* Expects players to level their buildings fast (level within 24h, if possible); either you have a huge daily collection or you have sufficient FP packages to ensure that.
* Might have more complex rules, e.g. to prevent snipers

The 1.8 Thread

* Is for easy-going players to take their time
* Lower level / less experienced players to get started
* Open to all guild-mates on request (if they are willing to read and follow the rules)

