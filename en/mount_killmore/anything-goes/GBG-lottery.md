# Guild Battle Ground
## Mechanics
### Conquer - Fight or Negotiate?
A successful fight counts as one point, a successful negotiation as two points. Both actions have the same impact on attrition.
To get more points with less attrition increase, negotiations are better. But for players with plenty and strong army troops, fighting might still be the better option.

### Attrition
The attrition is an individual factor to make contribution more expensive. It resets everyday at midnight (server time). Due to the individual attrition, GBG is a community effort: One player alone would suffer a huge attrition. Spreading the load on several players, the attrition is quite tolerable.

Per default, 10 actions will increase the attrition by 1. E.g.:

* For the first 10 negotiations, attrition will be 0, you will need one of each goods for each negotiation-attempt
* For the next 10 negotiations, attrition will be 1, you will need two of each goods for each negotiation-attempt
* For the next 10 negotiations, attrition will be 2, you will need three of each goods for each negotiation-attempt

When fighting, the strength of the enemy army will increase equivalently.

Buildings on the sector to be conquered or on neighboring own sectors can affect the attrition.  

**Note: One player doing 50 negotiations in one day might spend 2250 goods. 5 players doing 10 negotiations each, with the same amount of luck, would only spend 750 goods altogether. It’s cheaper to give away 600 goods to guild mates that couldn’t participate otherwise than doing all trades yourself. We might want to have an unfair trade channel for this purpose.**

### Buildings
There are 4 types of buildings:

* Increasing the number of points required to conquer a sector
* Increasing the attrition faster when attacking a sector
* Increasing number of points per hour on a sector
* Reducing attrition when attacking a neighboring sector

Buildings cost goods from the guild treasury. When a sector is conquered, there is a chance a building will survive or will be destroyed.

## Lottery
The lottery is sponsored by some guild-mates for the benefit of the guild. To participate, a player just needs to participate in GBG, no further investment is required.

* Each successful negotiation wins 2 tickets
* Each successful fight wins 1 ticket

Tickets are counted and prizes awarded at the end of the GBG.

There are three prizes, first prize is 50fp, second prize 20fp, third prize 10fp.

* 1st prize is drawn from all tickets
* Tickets of 1st prize winner are removed from the pool (each player can only win 1 prize)
* 2nd prize is drawn from remaining tickets
* Tickets of 2nd prize winner are removed from the pool
* 3rd prize is drawn from remaining tickets
