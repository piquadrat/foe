Welcome to ANYTHING GOES. We're a chilled guild. Play whichever way you want to (just make sure it is fair for all members). We help each other so we can all level up and enjoy playing the game together. Read this to understand how things work.

Our swap-threads are currently not in the Guild section, but in the Social section. You should be added to lower level swap threads on joining the guild, for higher ticket swaps and the special hunters swaps ask the guild leaders.

REGULAR SWAP THREADS: 
We have forge points (FP) swap threads to assist one another in leveling up our great buildings (GB). There are basic (5 and 10 fps) and high ticket (20, 50, and 100 fp) FP swap threads. The rules are simple for them. You just NEED to donate the required forge points to the last person who posted their of choice for each swap thread. 

SPECIAL GB BLUEPRINT (BP) FP SWAP: 
Every second building posted in the hunter threads should be the one the thread is about. E.g. every second building in the HC hunter thread should be a HC

**We do know that we said we're a chilled guild but please make sure you observe fairness in the FP swaps. We hope you can comply. **

GB IN THE MUD/CLOSE TO LEVEL: 
Help other guild members get their GBs built by donating any amount FP from the goodness of your heart. 

TRADES: 
Make sure your trades are fair (i.e., 1:1 same age goods; 2:1 for one level higher age goods/one level lower age goods). If your trades aren't fair and you are aware and you don't have any choice, give the members a heads up. We can accommodate such trades depending on our generosity.

1.9 Arc Support thread: 
If you have an Arc (or want one), you can ask to join the 1.9 Arc support thread. High-level members will provide free 1.9 support by taking 1st to 3rd place on Arcs posted there, low-level members provide free 1.9 support by taking 4th and 5th place to get cheap blueprints. 

Generic 1.9 Support thread: 
We have a generic 1.9 support thread. You can join on request, but you should have a level 80 Arc yourself to provide support as well.

GBG / GE: 
We join guild GBG and GE, for GE always level 1-4 are unlocked. Join if you want to. For GBG, there is a lottery that awards prices to participants. The more you did, the better your chance to win a price. It's entirely sponsored, you don't have to pay in at all.


Friends: 
Feel free to send friends request to guild members; we recommend to add friends from outside the guild as well, since those give additional opportunities to motivate/polish and to trade, but it is also common to add guild members as friends.

If you haven't been added to any of the guild threads, message GreasePanEyesBare, Leon, Hickory, sasha-builder or PiQuadrat.