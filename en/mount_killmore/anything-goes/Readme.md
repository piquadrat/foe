# Anything Goes

## Guild Threads

New users will be added to the most common guild thready by our leaders.
We have:

### Several FP swap threads.

New users will always be added to 5fp, 10fp, 15fp and 20fp swap threads.
Higher level users should be added to the 50fp and 100fp swap threads automatically as well,
but the decision is made by gut-feelign by whoever adds the user. 

Any user can be added on request.

***How it works***
To participate in the, say, 20fp swap-threads:

1. You post your building
2. You donate the 20fp to the building above you
3. The next one posting a building will donate to your building.


### GB Hunter Threads

We have hunter threads for Arc, Cape Canaveral and Himeji Castle

***How it works***
Basically like the other swap threads, but every second building posted must 
be the type the thread is dedicated to.

E.g. if the current building is an Arc in the Arc hunter thread, you can post any building you want,
donate to the Arc, and - if you make one of the top 5 places - hopefully get some Arc-blueprints in the end.

If the current building is *not* an Arc, however, you can only post an Arc (or wait until someone else does).

### Fair Trades
As the name says, this thread is for fair trades only.

***How it works***
1. Set up your fair trade in the market, where "Fair" means: 
    * 1:1 goods in the same era (e.g. 5 Lumber for 5 Dye)
    * 2:1 when trading an era up (e.g.10 Lumber for 5 Iron)
    * 1:2 when trading an era down (e.g. 5 Iron for 10 Lumber)
    ***Trades can only go maximum 1 era up***
2. Post your fair trades to the thread
    

### GBG Lottery

Participants in Guild Battle Grounds automatically participate for free in the 
[GBG Lottery](GBG-lottery.md)

